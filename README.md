# Blackbox Simple JS Client Example #

This repository contains a simple working example of how to connect, subscribe to, and query the Blackbox graph using javascript and websockets. 

### What is this repository for? ###

* Testing your Blackbox system setup quickly and easily.
* Demonstrating the basics for client connection and data polling. 

### How do I get set up? ###

* Clone this repository

    git clone https://bitbucket.org/blackai/blackbox-js-client-example.git

* Open the example.html in your browser
* Read through the code in example.html and example.js to understand implementation
* Begin writing your own queries against the user documentation
* Build on top of this codebase or write your own application
* (please) leave feedback on ways to improve the API or usage example!

### Building on this code example ###

* Ensure that for production implementations you are not authenticating against the Blackbox websocket endpoint on the client side. This was done here purely to keep the example app as simple as possible. 

### Who do I talk to? ###

* For urgent support call or email your Black team contact, or feel free to email us at contact@black.ai.