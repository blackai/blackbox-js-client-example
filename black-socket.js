/**
 * Created by karthik on 10/10/17.
 */
function bbSocketClient(protocol, domain, port, namespace) {

    this.protocol = protocol;
    this.domain = domain;
    this.port = port;
    this.namespace = namespace;
    this.socket = null;
    this.floormap = null;
    var ping_pong_times = [];
    this.start_time = (new Date()).getTime();
    this.subscribedTracks = {};

    // default callback functions
    this.onConnect = function(){
        $('.logs').append('<p>I"m connected</p>');
    };

    this.onFloorMap = function(msg){
        var message;
        if (msg.status === 'populated') {
            this.floormap = msg;
            message = '<p>floormap is populated</p>';
        }
        else if(msg.status === 'inprogress') {
            message = '<p>'+ msg.message +'</p>';
        }
        $('.floormap').append(message);
    };

    // track_history message has the following parameters.
    // {
    //   data: the trackids of tracks found in the system in the last specified interval
    this.onTrackHistory = function(msg){
        if (msg.status === 'OK'){
            $('.trackhistory').append('<p>' + msg.data + '</p>');
        }
    };

    this.onNewTrack = function(msg){
        $('.logs').append('<p>new track published: ' + msg.data + '</p>');
    };

    this.onSubscribedTrack = function(msg){
        var trackInfo = msg.data;
        if (!this.subscribedTracks[trackInfo.track_id]){
            this.subscribedTracks[trackInfo.track_id] = [];
        }
        this.subscribedTracks[trackInfo.track_id].push(trackInfo.position);
    };

    this.onExpiredTrack = function(msg){
        $('.logs').append('<p>Track(' + msg.data + ') expired</p>');
    };

    this.onMessage = function(msg){
        $('.logs').append(msg.message);
    };

    this.onActiveTracks = function (msg) {
      $('.activeTracks').text(msg.data);
    };

    this.onDisconnect = function(){
        $('.logs').append('<p>I"m disconnected</p>');
    };

    this.onPong = function(){
        var latency = (new Date()).getTime() - this.start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
        var sum = 0;
        for (var i = 0; i < ping_pong_times.length; i++) {
            sum += ping_pong_times[i];
        }
        $('.ping-pong').text(Math.round(10 * sum / ping_pong_times.length) / 10);
    };
    //

    // socket functions.

    this.connect = function () {

        $.ajax({
            type: "POST",
            url: 'http://' + this.domain + ':' + this.port + '/login',
            data: 'username=' + this.username + '&password=' + this.password,
            success: function(data) {
                console.log('success');
                console.log(data);
                event.preventDefault();
            },
            dataType: 'text'
        });

        this.socket = io.connect(this.protocol + '://' + this.domain + ':' + this.port + '/' + this.namespace);
    };

    this.getAllCallbacks = function(){
        return {
            trackHistoryCallback: this.onTrackHistory,
            subscribedTrackCallback: this.onSubscribedTrack,
            floormapCallback: this.onFloorMap,
            connectCallback: this.onConnect,
            disconnectCallback: this.onDisconnect,
            newTrackCallback: this.onNewTrack,
            expiredTrackCallback: this.onExpiredTrack,
            messageCallback: this.onMessage,
            activeTrackCallback: this.onActiveTracks
        };
    };

    this.setupCallbacks = function(callbacks){
        if (callbacks)
        {
            this.onTrackHistory = callbacks.trackHistoryCallback ? callbacks.trackHistoryCallback : this.onTrackHistory;
            this.onSubscribedTrack = callbacks.subscribedTrackCallback ? callbacks.subscribedTrackCallback : this.onSubscribedTrack;
            this.onFloorMap = callbacks.floormapCallback ? callbacks.floormapCallback : this.onFloorMap;
            this.onConnect = callbacks.connectCallback ? callbacks.connectCallback : this.onConnect;
            this.onDisconnect = callbacks.disconnectCallback ? callbacks.disconnectCallback : this.onDisconnect;
            this.onNewTrack = callbacks.newTrackCallback ? callbacks.newTrackCallback : this.onNewTrack;
            this.onExpiredTrack = callbacks.expiredTrackCallback ? callbacks.expiredTrackCallback : this.onExpiredTrack;
            this.onMessage = callbacks.messageCallback ? callbacks.messageCallback : this.onMessage;
        }

        this.socket.on('track_history',this.onTrackHistory);
        this.socket.on('subscribed_track',this.onSubscribedTrack);
        this.socket.on('floormap',this.onFloorMap);
        this.socket.on('connect',this.onConnect);
        this.socket.on('disconnect',this.onDisconnect);
        this.socket.on('new_track',this.onNewTrack);
        this.socket.on('expired_track',this.onExpiredTrack);
        this.socket.on('message',this.onMessage);
        this.socket.on('my_pong',this.onPong);
        this.socket.on('active_tracks',this.onActiveTracks);
    };

    this.getAllSubscribedTracks = function () {
        return this.subscribedTracks;
    };

    this.getActiveTracks = function () {
        if (!this.socket) {
            this.connect();
            this.setupCallbacks();
        }
        this.socket.emit('active_tracks', {});
    };

    this.getTrackHistory = function (durationInHours) {
        if (!this.socket) {
            this.connect();
            this.setupCallbacks();
        }
        this.socket.emit('track_history', {type:'duration', duration:{ last_interval: durationInHours}});
    };

    this.subscribeToTrack = function (track) {
        if (!this.socket) {
            this.connect();
            this.setupCallbacks();
        }
        this.socket.emit('subscribe', {topic: track, type: 'track'});
        $('.logs').append('<p>subscribed to track : ' + track + '</p>');
    };

    this.unsubscribeToTrack = function (track) {
        if (!this.socket) {
            this.connect();
            this.setupCallbacks();
        }
        this.socket.emit('unsubscribe', {topic: track, type: 'track'});
        $('.logs').append('<p>unsubscribed to track : ' + track + '</p>');
    };

    this.getFloorMap = function () {
        if (!this.socket) {
            this.connect();
            this.setupCallbacks();
        }
        this.socket.emit('subscribe', {topic: null, type: 'floormap'});
    };

    this.ping = function(){
        this.socket.emit('my_ping');
    };
}