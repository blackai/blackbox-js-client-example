/**
 * Created by karthik on 11/10/17.
 */
function Dashboard(){

        var handleFormEvents = function (socketClient) {
            $('form#getfloormap').submit(function (event) {
                socketClient.getFloorMap();
                return false;
            });
            $('form#getactivetracks').submit(function (event) {
                socketClient.getActiveTracks();
                return false;
            });
            $('form#subscribetotrack').submit(function (event) {
                socketClient.subscribeToTrack($('#subscribe_track').val());
                return false;
            });
            $('form#unsubscribetotrack').submit(function (event) {
                socketClient.unsubscribeToTrack($('#subscribe_track').val());
                return false;
            });

            $('form#trackhistory').submit(function (event) {
                socketClient.getTrackHistory($('#duration_in_hours').val());
                return false;
            });
            $('form#disconnect').submit(function (event) {
                socketClient.emit('disconnect_request');
                return false;
            });
        };

        var checkNetworkSpeed = function(socket){
          window.setInterval(function() {
                    socket.start_time = (new Date()).getTime();
                    socket.emit('my_ping');
                }, 1000);
        };

        // Blackbox websocket client parameters . provided by the black.ai team
        this.protocol = 'http';  // http or https; default: http
        this.domain = '52.65.9.248';      // api.black.ai/<your_application_url>
        this.port = '55955';          // default: 80
        this.bbNamespace = 'blackbox';    // applicationid !! You don't need to change this currently.

        // Populate Socket Client
        this.bbSocketClient = new bbSocketClient(this.protocol, this.domain, this.port, this.ethrNamespace);
        this.bbSocketClient.connect();

        // if callback methods aren't provided. default callbacks are used.
        // you could get the list of callbacks using this.bbSocketClient.getAllCallbacks()
        //you could pass your own callback functions into the setupCallbacks() method.
        // if callbacks aren't specified, default callbacks are used.
        // format:
        // this.onTrackHistory = function(msg){
        //          console.log(msg.data);
        //      };
        // this.bbSocketClient.setupCallbacks({
        //     trackHistoryCallback: this.onTrackHistory,
        //     subscribedTrackCallback: null,
        //     floormapCallback: null,
        //     connectCallback: null
        //     disconnectCallback: null,
        //     newTrackCallback: null,
        //     expiredTrackCallback: null,
        //     messageCallback: null
        // });
        this.bbSocketClient.setupCallbacks();
        handleFormEvents(this.bbSocketClient);
        checkNetworkSpeed(this.bbSocketClient.socket);
}
